import org.scalatest.{Matchers, FlatSpec}

import mupsit.io._



class StdOutTest extends FlatSpec with Matchers {


    class ThingWithoutConcreteDependency {
        self: OutputComponent =>
            def doSomething() = {
                self.outInit()
                self.outPrint("Nanananananana ")
                self.outPrintln("BATMAN!")
                self.outFinalize()
            }
    }

    val thing = new ThingWithoutConcreteDependency with StdOut

    thing.doSomething()

}
