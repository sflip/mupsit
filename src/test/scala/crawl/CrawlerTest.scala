import org.scalatest.{Matchers, FlatSpec}

import mupsit._
import mupsit.model._
import mupsit.crawl._




class CrawlerTest extends FlatSpec with Matchers {

    val config_dog      = new Config(Language.ENGLISH_UK, Language.GERMAN, "dog")
    val config_say      = new Config(Language.ENGLISH_UK, Language.GERMAN, "say")
    val config_lachen   = new Config(Language.GERMAN, Language.ENGLISH_UK, "lachen")
    val config_trompete = new Config(Language.GERMAN, Language.ENGLISH_US, "Trompete")

    "Leo " should " throw decent results" in {

        val crawler_dog      = new LeoCrawler(config_dog)
        val crawler_say      = new LeoCrawler(config_say)
        // val crawler_lachen   = new LeoCrawler(config_lachen)
        // val crawler_trompete = new LeoCrawler(config_trompete)

        val result_dog      = crawler_dog.crawl()
        val result_say      = crawler_say.crawl()
        // val result_lachen   = crawler_lachen.crawl()
        // val result_trompete = crawler_trompete.crawl()

        // debug
        // for (voc <- result_lachen)
            // println(voc.translation)

        // EXAM NOTE unit tests
        // EXAM NOTE the 'higher order function' functional approach for filter
        result_dog.filter(voc => voc.translation.contains("Hund")).length should be > 0
        result_say.filter(voc => voc.translation.contains("sagen")).length should be > 0
        // result_lachen.filter(voc => voc.translation.contains("laugh")).length should be > 0
        // result_trompete.filter(voc => voc.translation.contains("trumpet")).length should be > 0
    }



    "WordReference " should " throw decent results" in {

        val crawler_dog      = new WordreferenceCrawler(config_dog)
        val crawler_say      = new WordreferenceCrawler(config_say)
        val crawler_lachen   = new WordreferenceCrawler(config_lachen)
        val crawler_trompete = new WordreferenceCrawler(config_trompete)

        val result_dog      = crawler_dog.crawl()
        val result_say      = crawler_say.crawl()
        val result_lachen   = crawler_lachen.crawl()
        val result_trompete = crawler_trompete.crawl()

        // debug
        // for (voc <- result_lachen)
            // println(voc.translation)

        result_dog.filter(voc => voc.translation.contains("Hund")).length should be > 0
        result_say.filter(voc => voc.translation.contains("sagen")).length should be > 0
        result_lachen.filter(voc => voc.translation.contains("laugh")).length should be > 0
        result_trompete.filter(voc => voc.translation.contains("trumpet")).length should be > 0

    }



    "PONS " should " throw decent results" in {

        val crawler_dog      = new PonsCrawler(config_dog)
        val crawler_say      = new PonsCrawler(config_say)
        val crawler_lachen   = new PonsCrawler(config_lachen)
        val crawler_trompete = new PonsCrawler(config_trompete)

        val result_dog      = crawler_dog.crawl()
        val result_say      = crawler_say.crawl()
        val result_lachen   = crawler_lachen.crawl()
        val result_trompete = crawler_trompete.crawl()

        // debug
        // for (voc <- result_lachen)
            // println(voc.translation)

        result_dog.filter(voc => voc.translation.contains("Hund")).length should be > 0
        result_say.filter(voc => voc.translation.contains("sagen")).length should be > 0
        result_lachen.filter(voc => voc.translation.contains("laugh")).length should be > 0
        result_trompete.filter(voc => voc.translation.contains("trumpet")).length should be > 0

    }



    "Filter functions " should " do their job" in {

        val vocabs = Array(
            new Vocab("talk",     "reden",  Category.VERB),
            new Vocab("mountain", "Berg",   Category.NOUN),
            new Vocab("sit",      "sitzen", Category.VERB)
            )

        val config0 = new Config(
            filterCategory = None, limit = 0)
        val crawler0 = new LeoCrawler(config0)
        val vocabs0 = crawler0.filterVocabs(vocabs, config0)
        vocabs0.length should be (3)

        val config1 = new Config(
            filterCategory = Some(Category.VERB), limit = 0)
        val crawler1 = new LeoCrawler(config1)
        val vocabs1 = crawler1.filterVocabs(vocabs, config1)
        vocabs1.length should be (2)

        val config2 = new Config(
            filterCategory = Some(Category.NOUN), limit = 0)
        val crawler2 = new LeoCrawler(config2)
        val vocabs2 = crawler2.filterVocabs(vocabs, config2)
        vocabs2.length should be (1)
        vocabs2(0).translation should be ("Berg")

        val config3 = new Config(
            filterCategory = None, limit = 1)
        val crawler3 = new LeoCrawler(config3)
        val vocabs3 = crawler3.filterVocabs(vocabs, config3)
        vocabs3.length should be (1)
        vocabs3(0).translation should be ("reden")

        val config4 = new Config(
            filterCategory = Some(Category.NOUN), limit = 9999)
        val crawler4 = new LeoCrawler(config4)
        // val vocabs4 = crawler4.filterVocabs(vocabs, config4)
        val vocabs4 = crawler4.filterVocabs(vocabs) // config from crawler
        vocabs4.length should be (1)
        vocabs4(0).translation should be ("Berg")

    }

}
