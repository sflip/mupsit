package mupsit.io

import java.net.SocketTimeoutException
import java.net.ConnectException

import mupsit.Mupsit
import mupsit.model._
import mupsit.crawl._


// This defines the terminal user interface.
// It is responsible for parsing user input, handling it,
// hitting the crawler and showing the results.


// param args: args from Main
class TUI(val args: Array[String]) {
    self: OutputComponent =>

    override def toString = "TUI"

    //implicit val defaultIC = ImplicitChars("(",")"," <=> ")
    implicit val defaultIC = ImplicitChars("[","]",": ")

    // NOTE that there is a semantic difference between TUI options and
    //      class attributes of 'model.Config'.

    class MupsitOption
    (
        // long name of the option ('--name')
        // also the id of the option
        val nameLong     : String,

        // short name of the option ('-n')
        val nameShort    : String,

        // a one line description text
        val description  : String,

        // whether it takes a value ('-x value') or is a flag ('-x')
        val takesValue   : Boolean = false,

        // when takesValue == true, this should contain a function
        // that does a proper conversion
        val valueParser  : Option[String => Any] = None,

        // whether the option triggers something special (see MetaFunction)
        // None means it does not, Some should contain the MetaFunction
        val metaFunction : Option[MetaFunction.Value] = None,

        // whether the option is required
        val mandatory    : Boolean = false
    )

    val mupsitOptions : List[MupsitOption] = List(

        new MupsitOption(
            nameLong    = "from",
            nameShort   = "f",
            description = "The language to translate from.",
            takesValue  = true,
            valueParser = Some(Language.parseLanguage)
            ),

        new MupsitOption(
            nameLong    = "to",
            nameShort   = "t",
            description = "The language to translate to.",
            takesValue  = true,
            valueParser = Some(Language.parseLanguage)
            ),

        new MupsitOption(
            nameLong     = "languages",
            nameShort    = "L",
            description  = "List all available languages.",
            metaFunction = Some(MetaFunction.LANGUAGES)
            ),

        new MupsitOption(
            nameLong     = "help",
            nameShort    = "h",
            description  = "This help message.",
            metaFunction = Some(MetaFunction.HELP)
            ),

        new MupsitOption(
            nameLong    = "verbose",
            nameShort   = "v",
            description = "Print some additional information."
            ),

        new MupsitOption(
            nameLong    = "debug",
            nameShort   = "d",
            description = "Go into debug mode. For developers."
            ),

        new MupsitOption(
            nameLong    = "category",
            nameShort   = "c",
            description = "Restrict result to a specific category.",
            takesValue  = true,
            valueParser = Some(Category.parseCategory)
            ),

        new MupsitOption(
            nameLong     = "categories",
            nameShort    = "C",
            description  = "List all available categories.",
            metaFunction = Some(MetaFunction.CATEGORIES)
            ),

        new MupsitOption(
            nameLong    = "limit",
            nameShort   = "n",
            description = "Restrict the number of results." +
                          " 0 means not limit. " +
                          "Defaults to " + (new Config).limit + ".",
            takesValue  = true,
            valueParser = Some(Config.parseLimit)
            ),

        new MupsitOption(
            nameLong     = "version",
            nameShort    = "V",
            description  = "Show version number.",
            metaFunction = Some(MetaFunction.VERSION)
            )

    )


    // some options trigger meta functions (like '--help') and do not yield
    // a crawling config object.
    // The value is the id of the option flag
    object MetaFunction extends Enumeration {
        val HELP       = Value("help")
        val LANGUAGES  = Value("languages")
        val CATEGORIES = Value("categories")
        val VERSION    = Value("version")
    }


    // when TUI is instantiated, it should parse the given args and go for it
    parseInput() match
    {
        case Left(Left(config))        => Mupsit.config = config; goCrawl()
        case Left(Right(metafunction)) => parseMetafunction(metafunction)
        case Right(error)              => outPrintln(error)
    }

    private def parseMetafunction(mf : MetaFunction.Value) = {
        mf match {
            case MetaFunction.HELP       => printHelp()
            case MetaFunction.LANGUAGES  => printLanguages()
            case MetaFunction.CATEGORIES => printCategories()
            case MetaFunction.VERSION    => printVersion()
            case _ => throw new Exception("Unknown metafuntion!")
        }
    }


    // parse user input
    // (input is class variable 'args')
    // EXAM NOTE the usage of Either
    private def parseInput() : Either[Either[Config, MetaFunction.Value], String] =
    {
        if (args.length == 0) {
            return Right("No arguments specified! Try running with '--help'.")

        } else {
            // get default config
            // (we do not want do create config and update it afterwards because
            // its attributes are vals not vars, which is a cleaner approach)
            val defaultConfig : Config = new Config()
            var langOrigin      = defaultConfig.langOrigin
            var langTranslation = defaultConfig.langTranslation
            var query           = defaultConfig.query
            var verbose         = defaultConfig.verbose
            var debug           = defaultConfig.debug
            var filterCategory  = defaultConfig.filterCategory
            var limit           = defaultConfig.limit

            // run through args
            var isOptionValue = false // is this arg a value for an option?
            var optionWithValue : MupsitOption = null // the option for the latter
            for (arg <- args) { optionSearch(arg) match {
                // this arg starts an option
                case Some(option: MupsitOption) => {
                    // if we expect a value, it cannot be another option
                    if (isOptionValue) {
                        return Right("'" + arg + "' can not follow after '" +
                            optionWithValue.nameLong + "'")
                    }
                    // first catch meta functions
                    else if (option.metaFunction.isDefined) {
                        return Left(Right(option.metaFunction.get))
                    }
                    // whether a value will follow
                    else if (option.takesValue) {
                        isOptionValue = true
                        optionWithValue = option
                    }
                   // otherwise it must be a config option
                   else if (option.nameLong == "verbose") {
                       verbose = true
                   }
                   else if (option.nameLong == "debug") {
                       debug = true
                   }
                }
                // this arg does not start an option
                case None => {
                    if (isOptionValue) {
                        optionWithValue.nameLong match {
                            case "from" =>
                                optionWithValue.valueParser.get(arg) match {
                                    case Some(lang: Language.Value) =>
                                        langOrigin = lang
                                    case _ =>
                                        return Right(arg + " is an unknown language!")
                                }
                            case "to" =>
                                optionWithValue.valueParser.get(arg) match {
                                    case Some(lang: Language.Value) =>
                                        langTranslation = lang
                                    case _ =>
                                        return Right(arg + " is an unknown language!")
                                }
                            case "category" =>
                                optionWithValue.valueParser.get(arg) match {
                                    case Some(cat: Category.Value) =>
                                        filterCategory = Some(cat)
                                    case _ =>
                                        // return Right(arg + " is an unknown category!")
                                        filterCategory = None
                                }
                            case "limit" =>
                                optionWithValue.valueParser.get(arg) match {
                                    case Some(n: Int) =>
                                        limit = n
                                    case _ =>
                                        // return Right(arg + " is not a valid number!")
                                        limit = 0
                                }
                            case _ =>
                                return Right("Unimplemented Parser!")
                        }
                        isOptionValue = false
                        optionWithValue = null
                    }
                    else if (arg.startsWith("-")) {
                        return Right("Unknown option: '" + arg + "'.")
                    }
                    else {
                        query += (if (query.isEmpty) arg else " " + arg)
                    }
                }
            }}
            return if (query.isEmpty)
                Right("Empty input!")
                else Left(Left(new Config(
                                    langOrigin      = langOrigin,
                                    langTranslation = langTranslation,
                                    query           = query,
                                    verbose         = verbose,
                                    debug           = debug,
                                    filterCategory  = filterCategory,
                                    limit           = limit
                      )))
        }

        // convert a string option to a type safe mupsit option
        def optionSearch(string : String) : Option[MupsitOption] = {
            if (string.startsWith("--")) {
                val results = mupsitOptions.filter({
                    option => option.nameLong == string.substring(2)
                })
                if (results.length == 1)
                    Some(results(0))
                else
                    None
            }
            else if (string.startsWith("-")) {
                val results = mupsitOptions.filter({
                    option => option.nameShort == string.substring(1)
                })
                if (results.length == 1)
                    Some(results(0))
                else
                    None
            }
            else {
                None
            }
        }


        Right("foobar") // the compiler wants this unreachable code here
    }





    // use the crawlers and handle their output
    private def goCrawl() : Unit =
    {
        outPrintln("Searching for '" + Mupsit.config.query + "'...")
        outPrintln("")

        val crawlers = List(
            new LeoCrawler(Mupsit.config),
            new WordreferenceCrawler(Mupsit.config),
            new PonsCrawler(Mupsit.config)
            )

        for (crawler <- crawlers) {
            for (i <- 0 to 80)
                outPrint("#")
            outPrintln("")
            outPrintln("Fetching from " + crawler.name +
                                   " (" + crawler.url +"):\n")
            // if (crawler.name != "WordReference") { // TEST CONDITION
            try {
                val resultsUnfiltered = crawler.results // crawler.crawl()
                val results = crawler.filterVocabs(resultsUnfiltered)
                printResults(results)
                //printResults(results)(ImplicitChars("(",")"," : "))

            } catch {
                case e : LangNotSupportedException =>
                    outPrintln(e.description)
                case e : ConnectException    =>
                    outPrintln("Could not establish an internet connection.")
                case e : SocketTimeoutException    =>
                    outPrintln("Could not reach website. " +
                        "Do you have an internet connection?")
            }
            outPrintln("")
            // } // END TEST CONDITION
        }
    }



    // prints the given vocab array
    // EXAM NOTE implicit values for the way vocabs are printed
    private def printResults(vocabs: Array[Vocab])(implicit ic : ImplicitChars) =
    {
        if (vocabs.length > 0)
            vocabs.foreach{ voc =>
                outPrint(ic.open + voc.category + ic.close +" ")
                outPrint(voc.origin + ic.separator + voc.translation)
                outPrintln("")
            }
        else
            outPrintln("No results found.")
    }

    // print a nice little help
    private def printHelp() =
    {
        outPrintln("MUPSIT - A command line translation tool that scrapes web pages.")
        outPrintln("")
        outPrintln("Usage:  mupsit [OPTIONS] <TEXT>...")
        outPrintln("")
        outPrintln("<TEXT> is the text to be tranlated.")
        outPrintln("Every argument that is not an option will be part of <TEXT>.")
        outPrintln("If languages are not specified, defaults will be used.")
        outPrintln("")
        outPrintln("MUPSIT accepts the following OPTIONS:")
        for (option <- mupsitOptions) {
            outPrint("\t--" + option.nameLong)
            outPrint(if (option.takesValue) " VALUE" else "")
            outPrint(" | ")
            outPrint("-"  + option.nameShort)
            outPrintln(if (option.takesValue) " VALUE" else "")
            outPrint("\t\t" + option.description)
            outPrintln("")
        }
    }


    // print all available languages
    private def printLanguages() =
    {
        for (lang <- Language.values) {
            outPrintln(lang.toString)
        }
    }

    // print all available vocab categories
    private def printCategories() =
    {
        for (cat <- Category.values) {
            outPrintln(cat.toString)
        }
    }

    // print version number
    private def printVersion() =
    {
        outPrintln("MUPSIT Version " + Mupsit.version)
    }

}
