package mupsit.io

// This is an interface for an output module
// in order to use the cake pattern.
// It can be mixed in without specifying a concrete implementation.

// Currently, there are two traits that implement this interface:
//      - StdOut.scala
//      - FileOut.scala

trait OutputComponent {
    override def toString = "OutputComponent"

    // initialize the output medium
    // (i.e. create a file handler etc.)
    def outInit() : Boolean

    // write something (without linebreak)
    def outPrint(s: String) : Unit

    // write something with linebreak
    def outPrintln(s: String) : Unit

    // finish the output
    // (i.e. close a file handler etc.)
    def outFinalize() : Boolean

}
