package mupsit.io

// This is a concrete implementation of the output component.
// It prints to stdout.

// NOTE: We admit that this usage of Dependency Injections / Cake Pattern
// seems a little constructed. But hey, we understood and used it! Bazinga!


trait StdOut extends OutputComponent {

    def outInit() : Boolean = true

    def outPrint(s: String) : Unit = print(s)

    def outPrintln(s: String) : Unit = println(s)

    def outFinalize() : Boolean = true
}
