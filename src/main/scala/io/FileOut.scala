package mupsit.io

// This is a concrete implementation of the output component.
// It print to a file.


trait FileOut extends OutputComponent {
    val filepath: String
    val append: Boolean = false

    def outInit() : Boolean = false

    def outPrint(s: String) : Unit =
        throw new NotImplementedError("Sorry, use your shell file redirection.")

    def outPrintln(s: String) : Unit =
        throw new NotImplementedError("Sorry, use your shell file redirection.")

    def outFinalize() : Boolean = false

}
