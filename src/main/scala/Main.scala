package mupsit

import model._
import io._


object Mupsit {

    // mupsit software version
    lazy val versionMajor = 1
    lazy val versionMinor = 0
    lazy val version = versionMajor + "." + versionMinor

    // entry point
	def main(args: Array[String]): Unit = {
        // create a terminal user interface
        // EXAM NOTE: dependency injection with cake pattern
        val tui = new TUI(args) with StdOut
	}

    // Configuration for the crawling and stuff.
    // Will be written by the TUI instance.
    var config: Config = null


    // print something to stdout if we are in debug mode (or unknown mode)
    def logDebug(thing: Any) = {
        if (Mupsit.config == null || Mupsit.config.debug)
            println("[MUPSIT DEBUG] " + thing.toString)
    }

}



