package mupsit.crawl


import scala.collection.mutable.ArrayBuffer

import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
// import net.ruippeixotog.scalascraper.model.Document

import mupsit.Mupsit
import mupsit.model._


// the crawler for wordreference.com

class WordreferenceCrawler ( val config : Config ) extends Crawler {
    Mupsit.logDebug("new WordreferenceCrawler")

	val name = "WordReference"
	val url  = "http://www.wordreference.com"

    val allowedLangs = List(
        new LanguagePair(Language.ENGLISH_US, Language.GERMAN),
        new LanguagePair(Language.ENGLISH_UK, Language.GERMAN),
        new LanguagePair(Language.GERMAN, Language.ENGLISH_UK),
        new LanguagePair(Language.GERMAN, Language.ENGLISH_US)
    )


    // glue together the url to be fetched
    def buildFetchUrl() : String = {
        this.url + '/' +
        languageToUrlString(config.langOrigin) +
        languageToUrlString(config.langTranslation) + '/' +
        urlEncode(config.query)
    }


	// fetches result and parse them
	def fetchResults () : Array[Vocab] = {

        Mupsit.logDebug(s"fetch algorithm for $name")

        val fetchUrl = buildFetchUrl()
		val doc = browser.get(fetchUrl) // go for it!


        // BEAUTIFUL, BUT BUGGY WAY:
        // EXAM NOTE: yield and zip would have been awesome

/*
 *         val origins      = (doc >> ".FrWrd").map(element => element.toString)
 *         val translations = (doc >> ".ToWrd").map(element => element.toString)
 *
 *         println("from " + origins.length + " words to " + translations.length + " words")
 *         if (origins.length != translations.length)
 *             throw new Exception("Fuck, the zip gonna fail.")
 *
 *         (for ((origin, translation) <- origins.zip(translations))
 *             yield new Vocab(origin, translation)
 *         ).toArray
 */


        // WORKING WAY:

		// result
		var vocabs = ArrayBuffer.empty[Vocab]

        val rows = doc >> elementList(".WRD tr")

        // EXAM NOTE guard the loop to skip (i.e. 'continue') meaningless rows
        for (row <- rows; if row.attr("id") != "") {

            Mupsit.logDebug(s"Handling a row " + row.text)

            // dom element and string of origin text
            val e_origin  = row >> element(".FrWrd strong")
            val s_origin  = e_origin.text
            val origin    = s_origin.trim

            // dom elemen t and string of translation and category
            val e_trans   = row >> element(".ToWrd")
            val s_trans   = e_trans.text // contains the trailing category
            val e_cat     = e_trans >> element("em.POS2")
            val s_cat     = e_cat.text

            // category may have another span child we want to get rid of
            val cat       = if (e_cat.outerHtml.contains("<span"))
                                s_cat.substring(0, s_cat.lastIndexOf(
                                    (e_cat >> element("span")).text))
                            else
                                s_cat

            // in the translation string, get rid of category
            val trans = s_trans.substring(0, s_trans.lastIndexOf(s_cat))
                              .trim

            val category  = cat match {
                case "Nm"  => Category.NOUN
                case "Nf"  => Category.NOUN
                case "Npl" => Category.NOUN
                case "Nn"  => Category.NOUN
                case "n"   => Category.NOUN
                case "Vt"  => Category.VERB
                case "Vi"  => Category.VERB
                case "vi"  => Category.VERB
                case "iv"  => Category.VERB
                case "Adj" => Category.ADJ
                case "Adv" => Category.ADV
                case _     => Category.OTHER
            }


            // append new vocable
            vocabs += new Vocab(origin, trans, category)
        }

        Mupsit.logDebug(s"end fetch algorithm for $name")
        vocabs.toArray
	}

}
