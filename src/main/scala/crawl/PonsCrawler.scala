package mupsit.crawl


import scala.collection.mutable.ArrayBuffer

import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
// import net.ruippeixotog.scalascraper.model.Document

import mupsit.Mupsit
import mupsit.model._


// the crawler for pons.com

class PonsCrawler ( val config : Config ) extends Crawler {
    Mupsit.logDebug("new PonsCrawler")

    override val name = "PONS"
    override val url  = "http://en.pons.com"

    val allowedLangs = List(
        new LanguagePair(Language.ENGLISH_US, Language.FRENCH),
        new LanguagePair(Language.ENGLISH_US, Language.GERMAN),
        new LanguagePair(Language.ENGLISH_UK, Language.FRENCH),
        new LanguagePair(Language.ENGLISH_UK, Language.GERMAN),
        new LanguagePair(Language.FRENCH, Language.ENGLISH_UK),
        new LanguagePair(Language.FRENCH, Language.ENGLISH_US),
        new LanguagePair(Language.FRENCH, Language.GERMAN),
        new LanguagePair(Language.GERMAN, Language.ENGLISH_UK),
        new LanguagePair(Language.GERMAN, Language.ENGLISH_US),
        new LanguagePair(Language.GERMAN, Language.FRENCH)
    )


    // glue together the url to be fetched
    private def buildFetchUrl() : String = {

        // EXAM NOTE nested method definition for local use
        def buildDictString(lang1: Language.Value, lang2: Language.Value) = {
            if (lang1 == Language.FRENCH)
                languageToUrlString(lang2) ++ languageToUrlString(lang1)
            else if (lang2 == Language.FRENCH)
                languageToUrlString(lang1) ++ languageToUrlString(lang2)
            else if (lang1 == Language.GERMAN)
                languageToUrlString(lang1) ++ languageToUrlString(lang2)
            else if (lang2 == Language.GERMAN)
                languageToUrlString(lang2) ++ languageToUrlString(lang1)
            else
                throw new LangNotSupportedException("Invalid language.")
        }

        this.url + "/translate?" +
            "q="  + urlEncode(config.query) + "&" +
            // "lf=" + languageToUrlString(config.langOrigin) + "&" +
            "in=" + languageToUrlString(config.langOrigin) + "&" +
            "l="  + buildDictString(config.langOrigin, config.langTranslation)
    }


    // fetch result and parse it
    override def fetchResults : Array[Vocab] = {

        Mupsit.logDebug(s"fetch algorithm for $name")

        val fetchUrl = buildFetchUrl()
        val doc = browser.get(fetchUrl) // go for it!

        // result
        var vocabs = ArrayBuffer.empty[Vocab]

        val boxes = doc >> elementList(".results .entry .rom")
        for (box <- boxes) {

            // category (of box. contains multiple vocabs)
            // EXAM NOTE pattern matching to determine our type safe category
            val s_cat = box >?> text(".wordclass")
            val cat = s_cat match {
                case Some("NOUN") => Category.NOUN
                case Some("VERB") => Category.VERB
                case Some("ADJ" ) => Category.ADJ
                case Some("ADV" ) => Category.ADV
                case Some("CONJ") => Category.OTHER
                case Some("PREP") => Category.OTHER
                case _            => Category.OTHER
            }

            val rows = box >> elementList(".dl-horizontal")
            for (row <- rows) {

                val e_origin = row >> element(".source")
                val e_trans  = row >> element(".target")

                val s_origin = e_origin.text
                val s_trans  = e_trans.text

                // TODO clean algo
                // example: -f en -t de 'hat'
                // example: -f en -t de 'cookie'

                // some rows are not of our concern...
                if (!((e_origin >?> element("span.example")).isDefined ||
                      (e_origin >?> element("span.full_collocation")).isDefined ||
                      (e_origin >?> element("span.idiom_proverb")).isDefined ||
                      (e_origin >?> element("span.complement")).isDefined ||
                      (e_trans >?> element("span.explanation")).isDefined ||
                      (e_origin >?> element("span.grammatical_construction")).isDefined)) {

                    // ...others are

                    val origin = e_origin >?> element("span") match {
                        case Some(el) =>
                           s_origin.substring(0, s_origin.lastIndexOf(el.text))
                               .trim
                        case None     => s_origin.trim
                    }

                    val trans  = e_trans >?> element("span") match {
                        case Some(el) =>
                            if( el.attr("class") == "feminine" )
                                s_trans.substring(0, s_trans.lastIndexOf(el.text))
                                    .trim
                            else if ( el.attr("class") == "genus"
                                        && el.text == "f" ) {
                                var temp = s_trans.substring(0, s_trans.lastIndexOf(el.text))
                                    .trim
                                if(temp.lastIndexOf(" f") != -1)
                                    temp = temp.substring(0, temp.lastIndexOf("f"))
                                temp
                            }
                            else {
                                if (s_trans.lastIndexOf(" " + el.text) != -1)
                                    s_trans.substring(0, s_trans.lastIndexOf(" " + el.text))
                                        .trim
                                else s_trans
                            }
                        case None     => s_trans.trim
                    }

                    vocabs += new Vocab(origin, trans, cat)
                }

            }

        }

        Mupsit.logDebug(s"end fetch algorithm for $name")
        vocabs.toArray
    }
}
