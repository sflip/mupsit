package mupsit.crawl

import org.jsoup.HttpStatusException
import scala.collection.mutable.ArrayBuffer

import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._

import mupsit.Mupsit
import mupsit.model._


// the crawler for leo.org

class LeoCrawler ( val config : Config ) extends Crawler {
    Mupsit.logDebug("new LeoCrawler")

	val name = "LEO"
	val url = "http://dict.leo.org/dictQuery/m-vocab/" +
		languageToUrlString(config.langOrigin) +
		"de/de.html?search=" + urlEncode(config.query)
	val allowedLangs = List(
		new LanguagePair(Language.ENGLISH_US, Language.GERMAN),
		new LanguagePair(Language.ENGLISH_UK, Language.GERMAN)
	)


	// fetches result
	def fetchResults () : Array[Vocab] = {

        Mupsit.logDebug(s"fetch algorithm for $name")

		// the result buffer
		var vocabs = ArrayBuffer.empty[Vocab]

        try {
            // make request
            val doc = browser.get(url)

            // scrape word types
            val criterias = Array(
                ("#section-subst tr", Category.NOUN),
                ("#section-verb tr", Category.VERB),
                ("#section-adjadv tr", Category.ADJ)
            )

            for(criteria <- criterias) {
                // select the dom elements
                val rows = (doc >> elementList(criteria._1.toString))
                for (row <- rows) {
                    Mupsit.logDebug(s"Handling a row " + row.text)
                    var temp = row >> "td[data-dz-attr=relink]"

                    if(temp != Nil) {
                        val origin : String      = temp(0).trim
                        val trans : String       = temp(1).trim
                        val cat : Category.Value = criteria._2
                        vocabs += new Vocab(origin, trans, cat)
                    }
                }
            }
        } catch {
            // thrown if no results found
            case e : HttpStatusException => ()
        }

        Mupsit.logDebug(s"end fetch algorithm for $name")
		vocabs.toArray
	}

}
