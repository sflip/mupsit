package mupsit.crawl

// browser for webscraping
import net.ruippeixotog.scalascraper.browser.Browser
// url encoder
import java.net.URLEncoder
// own data types
import mupsit.Mupsit
import mupsit.model._



// Crawler main properties
trait Crawler {

	// the crawler identifier
	val name : String
    // the request configuration
    val config : Config
	// the url to request
	val url : String
    // allowed translations
    val allowedLangs : List[LanguagePair]

    // crawled results
    // (will be crawled only once and only when requested)
    // (EXAM NOTE that this MUST BE lazy. We will explain in the exam)
    lazy val results : Array[Vocab] = crawl()


    // initialize crawling
    // - if the crawler does not allow languagePair of given config,
    //   an exception is thrown
    // - else fetchResults is invoked and returned
    // (this could be done several times)
    def crawl() : Array[Vocab] = {
        Mupsit.logDebug(s"Initializing $name crawler.")

        if (translationValid())
            fetchResults()
        else
            throw new LangNotSupportedException(
                "%s does not support translations from %s to %s!"
                    .format(name, config.langOrigin, config.langTranslation))
    }


    // filter an array of vocabs according to a config
    // (filterCategory, limit, ...)
    // EXAM NOTE trait is more than an interface!
    //           we can define this 'static' method here
    // EXAM NOTE functional approach (not in situ)
    def filterVocabs(
        vocabs: Array[Vocab],
        config: Config = config) : Array[Vocab] = {

        // first, throw away every vocab that does not match the category
        val res : Array[Vocab] =
            if (config.filterCategory.isDefined)
                vocabs.filter(vocab =>
                        vocab.category == config.filterCategory.get)
            else
                vocabs

        // then apply limit
        if (config.limit == 0)
            res
        else
            res.take(config.limit)
    }



	// internal browser object
    // EXAM NOTE accessible in classes that mix in Crawler, but not public
	protected val browser = new Browser


	// sends a http request and parses the result
	// output: array of vocabs from different categories
	protected def fetchResults() : Array[Vocab]

    // check whether the language combination is valid
    private def translationValid() : Boolean = {
        allowedLangs.contains(new LanguagePair(
            config.langOrigin,
            config.langTranslation))
    }

	// standard language mapping
	// maybe override for different crawlers
    protected def languageToUrlString(lang: Language.Value) : String = {
        lang match {
            case Language.ENGLISH_US => "en"
            case Language.ENGLISH_UK => "en"
            case Language.GERMAN     => "de"
            case Language.FRENCH	 => "fr"
            case _                   => "en"
        }
    }

    // encodes a given url
    protected def urlEncode(urlToEncode: String) : String = {
    	URLEncoder.encode(urlToEncode)
    }


}
