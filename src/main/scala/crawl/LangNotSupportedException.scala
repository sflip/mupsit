package mupsit.crawl

// EXAM NOTE custom exception (will be catched in TUI)

class LangNotSupportedException(val description: String = "")
extends Exception {
    override def toString = "LangNotSupportedException" +
        (if (description == "") "" else " (" + description + ")")
}
