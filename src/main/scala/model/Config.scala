package mupsit.model

// the config class which is passed to the crawlers.
// it contains the successfully parsed arguments and options.
class Config
(

    // EXAM NOTE these are all constructor arguments.
    //           they have a default definition, so one can omit them.
    //           they are prefixed with val so they are class variables.


    // the language to translate from
    val langOrigin      : Language.Value
                        = Language.ENGLISH_US,

    // the language to translate to
    val langTranslation : Language.Value
                        = Language.GERMAN,

    // the text to find translations for
    val query           : String
                        = "",

    // whether verbose mode is enabled
    val verbose         : Boolean
                        = false,

    // whether debug mode is enabled
    val debug           : Boolean
                        = false,

    // Restrict the results...
    // (These restrictions will not be applied by Crawler.fetchResults
    // but by Crawler.filterResults)

    // ... to have only vocabs of a specific category
    // None:            No filter, allow all categories
    // Some Category:   Only allow this category
    val filterCategory  : Option[Category.Value]
                        = None,

    // .... to be not more than a specific number of Vocabs
    // limit = 0:       No limit, allow all we can get
    // limit > 0:       Only allow this many Vocabs
    val limit           : Int
                        = 9

)


object Config {

    // convert a string to an int
    // EXAM NOTE as a function object, this could be defined as a parameter!
    val parseLimit : String => Option[Int] =
    {
        s => try {
            val n = s.toInt
            if (n >= 0)
                Some(n)
            else
                None
        } catch {
            case e: Exception => None
        }
    }

}
