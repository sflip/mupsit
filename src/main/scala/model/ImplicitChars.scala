package mupsit.model

// case class used for delimiters in vocab printing (TUI)
// NOTE own case class
case class ImplicitChars
(
    open : String,
    close : String,
    separator : String
)
