package mupsit.model

// a language touple
class LanguagePair (

    // the language to translate from
    val from : Language.Value,

    // the language to translate to
    val to   : Language.Value

) {
	// own equality check
    // EXAM NOTE custom equality definition
	override def equals(o: Any) = o match {
    	case that: LanguagePair => that.from == this.from && that.to == this.to
    	case _ => false
  	}
}
