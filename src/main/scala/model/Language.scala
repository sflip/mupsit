package mupsit.model


// EXAM NOTE options for language type


// [ OPTION 1 ]
// class Language
//      We would need an extra companion object in order to use the case classes
//      without instantiating a Language object.
/*
 * class Language {
 *     case class ENGLISH_US() {}
 *     case class ENGLISH_UK() {}
 *     case class GERMAN() {}
 * }
 */


// [ OPTION 2 ]
// object Language
//      Language would not be a type.
/*
 * object Language {
 *     case class ENGLISH_US() {}
 *     case class ENGLISH_UK() {}
 *     case class GERMAN() {}
 * }
 */


// [ OPTION 3 ]
// traits as enum
//      Would be possible.
/*
 * trait Language
 *     case object ENGLISH_US extends Language
 *     case object ENGLISH_UK extends Language
 *     case object GERMAN extends Language
 */


// [ OPTION 4 ]
// Scala has a built in Enumeration class.
// We create a singleton object that extends Enumeration.
// This creates only a fourth of Java Byte code compared to OPTION 3
// (according to http://alvinalexander.com/scala/how-to-use-scala-enums-enumeration-examples)
// NOTE: This creates an object, not a type.
//       However, the according type can be obtained with 'Language.Value'.

object Language extends Enumeration {
    type Language = Value
    val ENGLISH_US = Value("EN_US")
    val ENGLISH_UK = Value("EN_UK")
    val GERMAN     = Value("DE")
    val FRENCH     = Value("FR")

    // turn a string description into a language
    val parseLanguage : String => Option[Language.Value] =
    {
        string =>
            // ignore case and check language
            string.toUpperCase match {
                case "DE"         => Some(Language.GERMAN)
                case "DEU"        => Some(Language.GERMAN)
                case "EN"         => Some(Language.ENGLISH_US)
                case "ENG"        => Some(Language.ENGLISH_US)
                case "ENGLISH"    => Some(Language.ENGLISH_US)
                case "ENGLISH_UK" => Some(Language.ENGLISH_UK)
                case "ENGLISH_US" => Some(Language.ENGLISH_US)
                case "EN_UK"      => Some(Language.ENGLISH_UK)
                case "EN_US"      => Some(Language.ENGLISH_US)
                case "FR"         => Some(Language.FRENCH)
                case "FRANCAIS"   => Some(Language.FRENCH)
                case "FRENCH"     => Some(Language.FRENCH)
                case "GB"         => Some(Language.ENGLISH_UK)
                case "GER"        => Some(Language.GERMAN)
                case "GERMAN"     => Some(Language.GERMAN)
                case "UK"         => Some(Language.ENGLISH_UK)
                case "US"         => Some(Language.ENGLISH_US)
                case _            => None
            }
    }
}
