package mupsit.model

// holds the basic data types
// may add category?
class Vocab (

	// the original word (may vary from the given query)
	val origin : String,

	// the translated word
	val translation : String,

	// (optional)
	var category : Category.Value = Category.OTHER

) {
	// prints a given Vocab
	override def toString(): String =
        " (" + category.toString + ") " + origin + " <=> " + translation
}

object Category extends Enumeration {
    val NOUN  = Value("noun")
    val VERB  = Value("verb")
    val ADJ   = Value("adjective")
    val ADV   = Value("adverb")
    val OTHER = Value("other")

    // turn a string into a category
    // EXAM NOTE we use the Option type to indicate that parsing might fail
    // EXAM NOTE this is a function object we can use anywhere
    val parseCategory : String => Option[Category.Value] =
    {
        string =>
            // ignore case and check language
            string.toUpperCase match {
                case "NOUN"  => Some(Category.NOUN)
                case "VERB"  => Some(Category.VERB)
                case "ADJ"   => Some(Category.ADJ)
                case "ADV"   => Some(Category.ADV)
                case "OTHER" => Some(Category.OTHER)
                case _       => None
            }
    }

}
