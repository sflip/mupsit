MUPSIT -  Mathias' und Philipp's Scala Implicit Translator
==========================================================

A command line translator that fetches translations from the web.
Written in Scala.


Description
-----------

With this tool, you can translate words from one language to another.

To get the results, the tool queries some websites:

- http://dict.leo.org
- http://wordreference.com
- http://pons.com

Currently supported languages are:

- English
- German
- French


Background
----------

This as a project for a university lecture:

Höhere Programmiersprachen: Scala
Lehr- und Forschungseinheit für Programmier- und Modellierungssprachen
Institut für Informatik der Ludwig-Maximilians-Universität München

WS 2015/16

[Lecture Website](http://www.pms.ifi.lmu.de/lehre/hoehereps/15ws16/index.html)


Installation
------------

This is a [sbt](http://www.scala-sbt.org/) project.

It can be compiled and executed via `sbt run`.
sbt should take care of the dependencies.

For further help, see the sbt documentation or send as a mail.


Usage
-----

In general, every argument passed to the program is considered a word of the
text to be translated. Exceptions are options like `--version ` or `-n 5`.

You can specify the languages with `--from DE` and `--to FR`. If you omit this,
the default languages (from english to german) are used. Hence, they are
*implicit*.

For further arguments and options, run with `--help`.


Authors
-------

Mathias Schlenker <matschlenker@gmail.com>
Philipp Moers <soziflip@gmail.com>




(C) Copyright 2016 Mathias Schlenker / Philipp Moers. All rights reserved.

